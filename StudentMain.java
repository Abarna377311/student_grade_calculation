package com.mile1.main;

import com.mile1.bean.Student;
import com.mile1.exception.NullMarksArrayException;
import com.mile1.exception.NullNameException;
import com.mile1.exception.NullStudentException;
import com.mile1.service.StudentReport;
import com.mile1.service.StudentService;

public class StudentMain {
	static Student data[]=new Student[4];
	static
	{
		for(int i=0;i<data.length;i++)
		data[i]=new Student();
		data[0]=new Student("Sekar",new int[] {35,35,35});
		data[1]=new Student(null,new int[] {11,22,33});
		data[2]=null;
		data[3]=new Student("Manoj",null);
	}
public static void main(String args[]) throws NullStudentException, NullNameException, NullMarksArrayException
{
	String grade="";
	int nullmarks,nullnames,nullobj;
	StudentReport sr=new StudentReport();
	for(int i=0;i<data.length;i++)
	{
	try
	{
	grade=sr.validate(data[i]);
	}
	catch(NullStudentException e)
	{
		grade="NullStudentException occured";
	}
	catch(NullNameException e)
	{
		grade="NullNameException occured";
	}
	catch(NullMarksArrayException e)
	{
		grade="NullMarksArrayException occured";
	}
	System.out.println("The student grade is :"+grade);
	}
	StudentService ss=new StudentService();
	nullmarks=ss.findNumberOfNullMarks(data);
	nullnames=ss.findNumberOfNullNames(data);
	nullobj=ss.findNumberOfNullObjects(data);
	System.out.println("Number of objects with marks array as null : "+nullmarks);
	System.out.println("Number of objects with name as null : "+nullnames);
	System.out.println("Number of objects that are entirely null : "+nullobj);
}
}
