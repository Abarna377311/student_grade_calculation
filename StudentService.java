package com.mile1.service;

import com.mile1.bean.Student;

public class StudentService {
public int findNumberOfNullMarks(Student data[])
{
	int count=0;
	String check;
	try
	{
		for(int i=0;i<data.length;i++)
		{
			if(data[i].getMarks()==null)
				throw new Exception();
		}
	}
	catch(Exception e)
	{
		count++;
	}
	return count;
}
public int findNumberOfNullNames(Student data[])
{
	int count=0;
	try
	{
		for(int i=0;i<data.length;i++)
		{
			if(data[i].getName()=="")
			throw new Exception();
		}
	}
	catch(Exception e)
	{
		count++;
	}
	return count;
}
public int findNumberOfNullObjects(Student data[])
{
	int count=0;
	try
	{
	for(int i=0;i<data.length;i++)
	{
		if(data[i]==null)
		throw new Exception();
	}
	}
	catch(Exception e)
	{
		count++;
	}
	return count;
}

}
