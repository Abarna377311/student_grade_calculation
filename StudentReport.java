package com.mile1.service;

import com.mile1.bean.Student;
import com.mile1.exception.NullMarksArrayException;
import com.mile1.exception.NullNameException;
import com.mile1.exception.NullStudentException;

public class StudentReport {
	String Grade;
	int sum=0;
	public String findGrade(Student studentObject)
	{
		for(int i=0;i<studentObject.getMarks().length;i++)
		{
			if(studentObject.getMarks()[i]<35)
			{
				Grade="F";
			}
			else
			{
				sum=sum+studentObject.getMarks()[i];
				if(sum<=150)
					Grade="D";
				else if(sum>=150&&sum<=200)
					Grade="C";
				else if(sum>=200&&sum<=250)
					Grade="B";
				else if(sum>=250&&sum<=300)
					Grade="A";
			}
		}
		//System.out.println("Grade "+Grade);
		return Grade;
	}
	public String validate(Student studentObject)throws NullStudentException,NullNameException,NullMarksArrayException
	{
		if(studentObject==null)
			throw new NullStudentException();
		else 
			if(studentObject.getName()==null)
			{
				throw new NullNameException();
			}
			else if(studentObject.getMarks()==null)
			{
				throw new NullMarksArrayException();
			}
			else
				return findGrade(studentObject);
	}
}
